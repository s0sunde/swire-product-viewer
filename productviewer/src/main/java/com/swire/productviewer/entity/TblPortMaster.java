package com.swire.productviewer.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Ravikiran Govindareddy
 */

@Entity
@Table(name = "tbl_port_master")
public class TblPortMaster {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="port_code")
	private String portCode ;
	
	@Column(name="port_name")
	private String portName ;
	
	@Column(name="city_name")
	private String cityName ;
	
	@Column(name="city_code")
	private String cityCode ;
	
	@Column(name="country_code")
	private String countryCode ;
	
	@Column(name="country_name")
	private String countryName ;
	
	@Column(name="port_time_zone")
	private String portTimeZone ;
	
	@Column(name="coordinate")
	private String coordinate ;

	public String getPortCode() {
		return portCode;
	}

	public void setPortCode(String portCode) {
		this.portCode = portCode;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getPortTimeZone() {
		return portTimeZone;
	}

	public void setPortTimeZone(String portTimeZone) {
		this.portTimeZone = portTimeZone;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((portCode == null) ? 0 : portCode.hashCode());
		result = prime * result + ((portName == null) ? 0 : portName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TblPortMaster other = (TblPortMaster) obj;
		if (portCode == null) {
			if (other.portCode != null)
				return false;
		} else if (!portCode.equals(other.portCode))
			return false;
		if (portName == null) {
			if (other.portName != null)
				return false;
		} else if (!portName.equals(other.portName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TblPortMaster [portCode=" + portCode + ", portName=" + portName + ", cityName=" + cityName
				+ ", cityCode=" + cityCode + ", countryCode=" + countryCode + ", countryName=" + countryName
				+ ", portTimeZone=" + portTimeZone + ", coordinate=" + coordinate + "]";
	}
		
}
