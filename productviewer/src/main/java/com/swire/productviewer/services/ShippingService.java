package com.swire.productviewer.services;

import org.springframework.http.ResponseEntity;

/**
 * 
 * @author Ravikiran Govindareddy
 */
public interface ShippingService {
    public ResponseEntity<?> loadOriginDestinationService() ;
}
