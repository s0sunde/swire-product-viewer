package com.swire.productviewer.services.impl;

import com.swire.productviewer.exception.ResourceNotFoundException;
import com.swire.productviewer.services.ShippingService;
import com.swire.productviewer.util.ConfigUtility;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * 
 * @author Ravikiran Govindareddy
 */
@Service
public class ShippingServiceImpl implements ShippingService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ShippingServiceImpl.class);

	@Autowired
	private ConfigUtility configUtility;

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public ResponseEntity<?> loadOriginDestinationService() {
		ResponseEntity<String> response = null ;
		try {
			String url = configUtility.getProperty("swire.trade.search.url");
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> request = new HttpEntity<String>(headers);
			response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);	
		}
		catch (Exception e) {
			LOGGER.error("Exception in ShippingServiceImpl class: " + e);
			throw new ResourceNotFoundException("Error");
		}
		return response;
	}

}
