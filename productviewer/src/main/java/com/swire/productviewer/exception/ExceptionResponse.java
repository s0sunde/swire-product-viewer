package com.swire.productviewer.exception;

import java.util.Date;
/**
 * 
 * @author Ravikiran Govindareddy
 */
public class ExceptionResponse {
	private Date timestamp;
	private String status;
	private String api;

	public ExceptionResponse(Date timestamp, String status, String api) {
		super();
		this.timestamp = timestamp;
		this.status = status;
		this.api = api;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getStatus() {
		return status;
	}

	public String getApi() {
		return api;
	}

}
