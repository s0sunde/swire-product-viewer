package com.swire.productviewer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
/**
 * 
 * @author Ravikiran Govindareddy
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ProductViewerException extends Exception {
	public ProductViewerException(String message) {
		super(message);
	}
}
