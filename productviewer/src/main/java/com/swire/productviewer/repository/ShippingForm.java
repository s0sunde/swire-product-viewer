package com.swire.productviewer.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.swire.productviewer.entity.TblPortMaster;

/**
 * 
 * @author Ravikiran Govindareddy
 */
@Repository
public interface ShippingForm extends CrudRepository<TblPortMaster, String> {

}
