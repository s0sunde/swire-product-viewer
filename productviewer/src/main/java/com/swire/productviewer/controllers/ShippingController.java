package com.swire.productviewer.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.swire.productviewer.exception.ResourceNotFoundException;
import com.swire.productviewer.services.ShippingService;

/**
 * 
 * @author Ravikiran Govindareddy
 */
@RestController
@RequestMapping("/swire/shippingFormService")
public class ShippingController {

	@Autowired
	ShippingService shippingService;

	@GetMapping(path = "/loadOriginDestination")
	public ResponseEntity<?> loadOriginDestinationDetails() throws ResourceNotFoundException {
		return shippingService.loadOriginDestinationService();	
	}
}
