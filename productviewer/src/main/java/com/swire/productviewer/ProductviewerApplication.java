package com.swire.productviewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;
/**
 * 
 * @author Ravikiran Govindareddy
 */
@EnableJpaRepositories
@SpringBootApplication
public class ProductviewerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductviewerApplication.class, args);
	}
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}

}
