package com.swire.productviewer.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * Spring Environment utility class 
 *
 * @author Ravikiran Govindareddy
 */

@Configuration
public class ConfigUtility {

	@Autowired
	private Environment environment;

	public String getProperty(String pPropertyKey) {
		return environment.getProperty(pPropertyKey);
	}
}
